<?php
/**
 * Plugin Name: Did Documentation
 * Plugin URI: http://did-web.com
 * Description:  General help service for the basic management of Wordpress. Oriented to Editors and authors.
 * Version: 1.0.0
 * Author: Eduard Pinuaga - Did-web.com
 * Author URI: hhttp://did-web.com
 * Requires at least: 5.1
 * Tested up to: 4.9.8
 *
 * Text Domain: didcontenidosporpais
 * Domain Path: /languages/
 */
defined('ABSPATH') or die('Without permission');
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
add_action('admin_menu', 'didd_admin_menu');

function didd_admin_menu()
{
    add_menu_page(
          'Vídeos de Ayuda',        // Title Page
          'Vídeos de Ayuda',        // Title menu
          'publish_pages',          // Rol access
          'videos-de-ayuda',         // Slug
          'diddHomeConfig',       // function configuration plugin
          'dashicons-editor-help',                           // Icon
          90                            // Position
          );
}


function diddHomeConfig($datas)
{
    include_once(__DIR__.'/templates/admin/didd_admin_home.php');
}
