<div class="wrap">

<h1><span class="dashicons dashicons-video-alt"></span> Vídeos de Ayuda</h1>
<p>
  Vídeos para la ayuda del Author y editro de Wordpress.
</p>
<hr />

<hr />
<br /><br /><br /><br />
<h2 class="nav-tab-wrapper">Cómo crear una entrada en Wordpress?</h2>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/W-QNfJ2RvhU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<hr />
<br /><br /><br /><br />
<h2 class="nav-tab-wrapper">Cómo crear un enlace en una entrada de Wordpress 5.1?</h2>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/Rh11oWkugNo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<hr />
<br /><br /><br /><br />
<h2 class="nav-tab-wrapper">Cómo mejorar el SEO de una entrada con Yoast SEO, en Wordpress 5.1 ?</h2>
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/Npzc7_JPKpA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</div>
